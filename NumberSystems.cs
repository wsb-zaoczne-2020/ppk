﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace PPK_LAB
{
    class NumberSystems
    {
        private static readonly Dictionary<int, string> systemsNumbersString = new Dictionary<int, string>{
            { 0, "0" },
            { 1, "1" },
            { 2, "2" },
            { 3, "3" },
            { 4, "4" },
            { 5, "5" },
            { 6, "6" },
            { 7, "7" },
            { 8, "8" },
            { 9, "9" },
            { 10, "A" },
            { 11, "B" },
            { 12, "C" },
            { 13, "D" },
            { 14, "E" },
            { 15, "F" },
            { 16, "G" },
            { 17, "H" },
            { 18, "I" },
            { 19, "J" },
            { 20, "K" },
            { 21, "L" },
            { 22, "M" },
            { 23, "N" },
            { 24, "O" },
            { 25, "P" },
            { 26, "R" },
            { 27, "S" },
            { 28, "T" },
            { 29, "U" },
            { 30, "W" },
            { 31, "X" },
            { 32, "Y" },
            { 33, "Z" }
        };

        private static readonly Dictionary<string, int> systemsNumbersInt = new Dictionary<string, int>{
            { "0", 0 },
            { "1", 1 },
            { "2", 2 },
            { "3", 3 },
            { "4", 4 },
            { "5", 5 },
            { "6", 6 },
            { "7", 7 },
            { "8", 8 },
            { "9", 9 },
            { "A", 10 },
            { "B", 11 },
            { "C", 12 },
            { "D", 13 },
            { "E", 14 },
            { "F", 15 },
            { "G", 16 },
            { "H", 17 },
            { "I", 18 },
            { "J", 19 },
            { "K", 20 },
            { "L", 21 },
            { "M", 22 },
            { "N", 23 },
            { "O", 24 },
            { "P", 25 },
            { "R", 26 },
            { "S", 27 },
            { "T", 28 },
            { "U", 29 },
            { "W", 30 },
            { "X", 31 },
            { "Y", 32 },
            { "Z", 33 }
        };

        private static readonly List<int> numberSystems = new List<int>{
            { 2 },
            { 3 },
            { 4 },
            { 5 },
            { 6 },
            { 7 },
            { 8 },
            { 9 },
            { 10 },
            { 11 },
            { 12 },
            { 13 },
            { 14 },
            { 15 },
            { 16 },
            { 17 },
            { 18 },
            { 19 },
            { 20 },
            { 21 },
            { 22 },
            { 23 },
            { 24 },
            { 25 },
            { 26 },
            { 27 },
            { 28 },
            { 29 },
            { 30 },
            { 31 },
            { 32 },
            { 33 },
            { 34 },
            { 35 },
            { 36 }
        };

        public void Run()
        {
            Console.WriteLine("Proszę podać system liczbowy z jakiego będziemy przeliczać: ");
            int.TryParse(Console.ReadLine(), out int translateFromSystem);
            Console.WriteLine("Podaj liczbę do przeliczenia w systemie " + translateFromSystem + ": ");
            string numberString = Console.ReadLine();

            foreach (int translateToSystem in numberSystems)
            {
                string answer = "";
                if (translateFromSystem == 10)
                {
                    Double.TryParse(numberString, out double number);
                    answer += DecimalTo(translateToSystem, number);
                }

                if (translateFromSystem != 10)
                {
                    if (numberString.Contains(","))
                    {
                        string leftSide = numberString.Split(",")[0];
                        string rightSide = numberString.Split(",")[1];
                        answer += ToDecimalLeftSide(leftSide, translateFromSystem);
                        answer += "," + ToDecimalRightSide(rightSide, translateFromSystem);
                    }

                    if (!numberString.Contains(","))
                    {
                        answer += ToDecimalLeftSide(numberString, translateFromSystem);
                    }

                    answer = DecimalTo(translateToSystem, double.Parse(answer)).ToString();
                }

                Console.WriteLine("Answer in " + translateToSystem + ": " + answer);
            }
        }

        private string DecimalTo(int translateToSystem, double number)
        {
            int leftPart = (int)number;
            double rightPart = number % 1;
            string answer = TranslateLeftPartDecimal(leftPart, translateToSystem);
            if (rightPart != 0)
            {
                answer += "." + TranslateRightPartDecimal(rightPart, translateToSystem);
            }

            return answer;
        }

        private string TranslateLeftPartDecimal(int number, int system)
        {
            string result = "";

            while (number > 0)
            {
                int restOfDivision = number % system;
                if (number < system)
                {
                    restOfDivision = number;
                }

                if (system > 10)
                {
                    result += systemsNumbersString[restOfDivision];
                }

                if (system < 11)
                {
                    result += restOfDivision;
                }
                number /= system;
            }
            return Reverse(result);
        }

        private string TranslateRightPartDecimal(double number, int system)
        {
            int l = number.ToString().Split(",")[1].Length;
            string b = "";

            for (int i = 0; i < l; i++)
            {
                int z = (int)(number * system);
                b += systemsNumbersString[z];
                number = (number * system) - z;
            }
            return b;
        }

        public double ToDecimalLeftSide(string binaryNumber, int system)
        {
            char[] binaryNumberArray = binaryNumber.ToCharArray();
            Array.Reverse(binaryNumberArray);
            double result = 0;
            for (int i = 0; i < binaryNumberArray.Length; i++)
            {
                int value = (int)systemsNumbersInt[binaryNumberArray[i].ToString()];
                result += (value * Math.Pow(system, i));
            }

            return result;
        }

        public double ToDecimalRightSide(string binaryNumber, int system)
        {
            char[] reversed = binaryNumber.ToCharArray();
            double result = 0;
            for (int i = 0; i < reversed.Length; i++)
            {
                int.TryParse(reversed.GetValue(i).ToString(), out int value);
                result += value * Math.Pow(system, -(i + 1));
            }

            return result;
        }

        private string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}

﻿using System;

namespace PPK_LAB
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Wybierz program:");
            Console.WriteLine("1 = Number Systems");
            Console.WriteLine("2 = Lab1");
            int.TryParse(Console.ReadLine(), out int choice);
            switch (choice)
            {
                case 1:
                    new NumberSystems().Run();
                    break;
                case 2:
                    new Lab1().Tasks();
                    break;
                default:
                    break;
            }
        }
    }
}
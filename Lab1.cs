﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace PPK_LAB
{
    class Lab1
    {
        public void Tasks()
        {
            Console.WriteLine("Które zadanie chcesz sprawdzić 1, 2 czy 3?");
            Console.WriteLine("Nie wybranie żadnej opcji spowoduje uruchomienie wszystkich zadań po kolei.");
            switch (Console.ReadLine())
            {
                case "1":
                    Task1();
                    break;
                case "2":
                    Task2();
                    break;
                case "3":
                    Task3();
                    break;
                default:
                    Task1();
                    Task2();
                    Task3();
                    break;
            }
        }

        /**
         * Napisz program pozwalający na wyznaczanie rzeczywistych pierwiastków równania kwadratowego.
         * Program powinien dać możliwość wprowadzenia współczynników a,b,c równania kwadratowego.
         * Po wprowadzeniu danych program powinien wypisać na ekranie pierwiastki.
         */
        private void Task1()
        {
            double x1 = 0.0;
            double x2 = 0.0;
            Console.WriteLine("Podaj liczby do równania kwadratowego.");
            Console.ReadLine();
            Console.WriteLine("Podaj wartość dla a: ");
            double.TryParse(Console.ReadLine(), out double a);
            if (a == 0)
            {
                Console.WriteLine("a musi być różne od zera");
                Task1();
                return;
            }
            Console.WriteLine("Podaj wartość dla b: ");
            double.TryParse(Console.ReadLine(), out double b);
            Console.WriteLine("Podaj wartość dla c: ");
            double.TryParse(Console.ReadLine(), out double c);

            double delta = Math.Pow(b, 2.0) - (4 * a * c);

            if (delta < 0)
            {
                Console.WriteLine("Delta mniejsza niż 0, równanie nie ma żadnych pierwiastkó");
                return;
            }

            if (delta == 0)
            {
                x1 = -b / 2 * a;
            }

            if (delta > 0)
            {
                x1 = (-b - Math.Sqrt(delta)) / 2 * a;
                x2 = (-b + Math.Sqrt(delta)) / 2 * a;
            }
            Console.WriteLine("x1=" + x1);
            Console.WriteLine("x2=" + x2);
        }

        /**
         * Napisać program wyznaczający wartość n!
         * a) Z zastosowaniem metody rekurencyjnej
         * b) Z zastosowaniem metody iteracyjnej.
         */
        private void Task2()
        {
            Console.WriteLine("Podaj liczbę naturalną z której ma być liczona silnia");
            BigInteger.TryParse(Console.ReadLine(), out BigInteger n);
            Console.WriteLine("Wynik wykorzystując rekurencję " + n + "! = " + FactorialRecurtion(n));
            Console.WriteLine("Wynik wykorzystując iterację " + n + "! = " + FactorialIteration(n));
        }

        private BigInteger FactorialRecurtion(BigInteger n)
        {
            if (n < 1)
            {
                return 1;
            }

            return n * FactorialRecurtion(n - 1);
        }

        private BigInteger FactorialIteration(BigInteger n)
        {
            BigInteger result = 1;
            for (BigInteger i = 1; i <= n; i++)
            {
                result *= i;
            }

            return result;
        }

        /**
         * Napisać program pozwalający na implementację algorytmu wydawania. 
         * Program ma umożliwić wprowadzenie kwoty jaka należy wydać, oraz pokazać na ekranie nominały banknotów do wydania. 
         * Należy przyjąć że wydajemy następujące banknoty 200, 100, 50, 20, 10. 
         * Na początku należy sprawdzić czy kwota wprowadzona przez użytkownika może być wydana za pomocą nominałów jakie mamy do dyspozycji.
         */
        private void Task3()
        {
            var possibleChange = new List<double>() {
                10,
                20,
                50,
                100,
                200
            };

            Console.WriteLine("Podaj resztę do wypłacenia:");
            double.TryParse(Console.ReadLine(), out double change);
            possibleChange.Sort(new SortDoubleDescending()); //be sure of the order
            double min = possibleChange[^1];//last value is the smallest one

            if (change == 0 || change % min != 0)
            {
                Console.WriteLine("Przykro nam ale nie możemy wydać reszty dostępnymi nominałami.");
                Task3();
                return;
            }

            string result = "Reszta:\n";
            for (int i = 0; ((i < possibleChange.Count) && (change > 0)); i++)
            {
                if (change >= possibleChange[i])
                {
                    int multiplier = (int)(change / possibleChange[i]);
                    result += possibleChange[i] + " $ x " + multiplier + "\n";
                    change -= (multiplier * possibleChange[i]);
                }
            }

            Console.WriteLine(result);
        }

        private class SortDoubleDescending : IComparer<double>
        {

            int IComparer<double>.Compare(double a, double b)
            {
                if (a > b)
                    return -1;
                if (a < b)
                    return 1;
                else
                    return 0;
            }
        }
    }
}